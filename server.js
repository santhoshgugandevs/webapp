const http = require('http');
const fs = require('fs');

const port = 3000;
const server = http.createServer((req,res) =>{

    if(req.url == "/"){
        const indexFile = 'index.html';
        fs.stat(`./${indexFile}`, (err, stats) => {
            // console.log("file status : ", stats, indexFile);
            if(stats){
                res.statusCode = 200;
                res.setHeader('Content-Type','text/html');
                fs.createReadStream(indexFile).pipe(res);
            }else{
                res.statusCode = 404;

                res.setHeader('Content-Type','text/html');
                res.end('<h1>Page not Found</h1>');
            }
        });
    }
});

server.listen(port, (status) => {
    console.log("Web App for website running on http://localhost:"+port);
});
